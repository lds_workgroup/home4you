const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const PORT = 3000;
const admin = require('./controllers/adminController');

var expressSanitizer = require('express-sanitizer');
const app = express();
const routes = require('./routes/routes');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressSanitizer());


app.use(express.static(path.join(__dirname, 'public')));
app.set('view engine', 'pug');
app.engine('html', require('ejs').renderFile);
app.set('views', './views');


app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false
}));

app.use('/', routes);

var MongoClient = require('mongodb').MongoClient ,assert = require('assert');
var url = 'mongodb://localhost:27017/home4you';//origem da BD
MongoClient.connect(url,  { useNewUrlParser: true },function(err, db) {
    assert.equal(null, err);
    console.log("Conectado à base de dados");
    db.close();
});

app.listen(PORT, () => {
    console.log('À escuta na porta ' + PORT);
});

app.get('/', (req, res) => {
    admin.getAllAdmins(req, (err, user)=>{
        if(err){
            next(err);
        }else if(user.length > 0){
            console.log('Já existe');
        }else{
            admin.addInitialAdmin(req, (err, res)=>{
                if(err){
                    console.log(err);
                    throw err;
                }
            });
        }
    })
    res.render(__dirname + '/views/login.pug');
});