const { check, validator } = require("express-validator/check");
const funcionarioModel = require("../models/funcionarioModel");
const bcrypt = require("bcryptjs");
const saltRounds = 10;

/**
 * Função responsavel por validar/sanitizar dados, encriptar palavra passe, de um funcionario e adicionar à base de dados
 * @param {*} req Request
 * @param {*} callback callback Erro ou false caso os dados não sejam validos
 */

function addFuncionario(req, callback) {
  let nome = req.sanitize(req.body.nome);
  let username = req.sanitize(req.body.username);
  let pass = req.sanitize(req.body.pass);

  if (nome && username && pass) {
    bcrypt.hash(pass, saltRounds, function(err, hash) {
      let funcionario = new funcionarioModel(nome, username, hash);
      funcionarioModel.addFuncionario(funcionario.thisObject(), err => {
        callback(err);
      });
    });
  } else {
    callback(false);
  }
}

/**
 * Função responsavel por validar/sanitizar dados, encriptar palavra passe, de um funcionario e adicionar á base de dados
 * @param req Request
 * @param callback callback Erro ou false caso os dados não sejam validos
 */
function editarFuncionario(req, callback) {
  let username = req.sanitize(req.body.username);
  let pass = req.sanite(req.body.pass);
  let tipo = req.sanitize(req.body.tipo);
  if (username && pass && tipo) {
    bcrypt.hash(pass, saltRounds, function(err, hash) {
      let query = { nome: nome };
      let values = { $set: { username: username, pass: hash, tipo: tipo } };
      funcionarioModel.updateFuncionario(query, values, err => {
        if (err) {
          callback(err);
        }
        callback(hash);
      });
    });
  } else {
    callback(false);
  }
}

/**
 * Função responsavel por validar/sanitizar dados, encriptar palavra passe, de um funcionario e adicionar á base de dados
 * @param req Request
 * @param callback callback Erro ou false caso os dados não sejam validos
 */
function eliminarFuncionario(req, callback) {
  let username = req.params.id;

  if (username) {
    funcionarioModel.deleteFuncionario(username, (err) => {
      if (err) {
        callback(err);
      }
    });
  } else {
    callback(false);
  }
}

/**
 * Função responsavel por verificar se a palavra passe introduzida é igual à hash presente na base de dados, permitindo
 * assim o login.
 * @param req Request
 * @param callback Match caso sejam iguas, false caso contrario
 */
function checkData(req, callback) {
  let username = req.sanitize(req.body.username);
  let pass = req.sanitize(req.body.pass);
  funcionarioModel.getFuncionarioByUsername(username, (err, resultado) => {
    if (resultado != undefined) {
      let dbPass = resultado.pass;
      if (username && pass) {
        bcrypt.compare(pass, dbPass, function(err, match) {
          req.session.username = resultado.username;
          req.session.tipo = resultado.tipo;
          req.session.nome = resultado.nome;
          callback(match);
        });
      } else {
        callback(false);
      }
    } else {
      callback(false);
    }
  });
}

function getFuncionarioByNome(req, callback) {
  let nome = req.sanitize(req.body.nome);
  if (nome) {
    funcionarioModel.getFuncionarioByNome(nome, (err, res) => {
      callback(err, res);
    });
  }
}

function getFuncionarioByUsername(req, callback) {
  let username = req.sanitize(req.body.username);
  if (username) {
    funcionarioModel.getFuncionarioByUsername(username, (err, res) => {
      callback(err, res);
    });
  }
}

function getFuncionarios(req, callback) {
  funcionarioModel.getAllFuncionarios((err, res) => {
    callback(err, res);
  });
}

exports.addFuncionario = addFuncionario;
exports.editarFuncionario = editarFuncionario;
exports.eliminarFuncionario = eliminarFuncionario;
exports.checkData = checkData;
exports.getFuncionarioByNome = getFuncionarioByNome;
exports.getFuncionarioByUsername = getFuncionarioByUsername;
exports.getFuncionarios = getFuncionarios;