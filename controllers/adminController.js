const { check, validator } = require("express-validator/check");
const adminModel = require("../models/adminModel");
const bcrypt = require("bcryptjs");
const saltRounds = 10;

/**
 * Função responsavel por validar/sanitizar dados, encriptar palavra passe, de um admin e adicionar à base de dados
 * @param {*} req Request
 * @param {*} callback callback Erro ou false caso os dados não sejam validos
 */

function addAdmin(req, callback) {
  let username = req.sanitize(req.body.username);
  let pass = req.sanitize(req.body.pass);
  let tipo = req.sanitize(req.body.tipo);

  if (username && pass && tipo) {
    bcrypt.hash(pass, saltRounds, function(err, hash) {
      let admin = new adminModel(username, hash, tipo);
      adminModel.addAdmin(admin.thisObject(), err => {
        callback(err);
      });
    });
  } else {
    callback(false);
  }
}

function addInitialAdmin(req, callback) {
  let username = "admin";
  let pass = "admin";
  let tipo = 1;

  if (username && pass && tipo) {
    bcrypt.hash(pass, saltRounds, function(err, hash) {
      let admin = new adminModel(username, hash, tipo);
      adminModel.addAdmin(admin.thisObject(), err => {
        callback(err);
      });
    });
  } else {
    callback(false);
  }
}

/**
 * Função responsavel por verificar se a palavra passe introduzida é igual à hash presente na base de dados, permitindo
 * assim o login.
 * @param req Request
 * @param callback Match caso sejam iguas, false caso contrario
 */
function checkData(req, callback) {
  let username = req.sanitize(req.body.username);
  let pass = req.sanitize(req.body.pass);
  adminModel.getAdminByUsername(username, (err, resultado) => {
    if (resultado != undefined) {
      let dbPass = resultado.pass;
      if (username && pass) {
        bcrypt.compare(pass, dbPass, function(err, match) {
          req.session.username = resultado.username;
          req.session.tipo = resultado.tipo;
          callback(match);
        });
      } else {
        callback(false);
      }
    }else{
        callback(false);
    }
  });
}

function getAdminByUsername(req, callback) {
  let username = req.sanitize(req.body.username);
  if (username) {
    adminModel.getAdminByUsername(username, (err, res) => {
      callback(err, res);
    });
  }
}

function getAllAdmins(req, callback) {
  adminModel.getAllAdmins((err, res) => {
    callback(err, res);
  });
}

exports.addAdmin = addAdmin;
exports.addInitialAdmin = addInitialAdmin;
exports.checkData = checkData;
exports.getAdminByUsername = getAdminByUsername;
exports.getAllAdmins = getAllAdmins;
