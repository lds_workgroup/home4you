const { check, validator } = require("express-validator/check");
const requerimentoModel = require("../models/requerimentoModel");

/**
 * Função responsavel por adicionar um requerimento à base de dados, validando e sanitizando os valores introduzidos
 * @param {*} req
 * @param {*} callback
 */
function addRequerimento(req, callback) {
  let id_aluno = req.sanitize(req.session.username);
  let titulo = req.sanitize(req.body.titulo);
  let descricao = req.sanitize(req.body.descricao);
  let data = new Date();

  if (id_aluno && titulo && descricao && data) {
    let requerimento = new requerimentoModel(id_aluno, titulo, descricao, data);
    requerimentoModel.addRequerimento(requerimento.thisObject(), err => {
      callback(err);
    });
  } else {
    callback(false);
  }
}

function atualizarRequerimento(req, callback) {
  let id_aluno = req.sanitize(req.session.username);
  let titulo = req.sanitize(req.body.titulo);
  let descricao = req.sanitize(req.body.descricao);
  let data = new Date();
  let estado = req.sanitize(req.body.estado);
  if (id && id_aluno && titulo && descricao && data && estado) {
    let query = { id: id };
    let values = { $set: { estado: estado } };
    requerimentoModel.updateRequerimento(query, values, err => {
      if (err) {
        callback(err);
      }
    });
  } else {
    callback(false);
  }
}

function alterarEstadoRequerimento(req, callback) {
  let id_req = req.sanitize(req.params.id);
  let novoEstado = req.sanitize(req.params.estado);

  requerimentoModel.getRequerimentosPorID(id_req, (err, resultado) => {
    let query = { _id: resultado._id };
    console.log(query);
    let values = {
      $set: {
        id_aluno: resultado.id_aluno,
        titulo: resultado.titulo,
        descricao: resultado.descricao,
        data: resultado.data,
        estado: parseInt(novoEstado)
      }
    };
    console.log(values);
    requerimentoModel.updateRequerimento(query, values, err => {
      if (err) {
        callback(err);
      }
      callback(true);
    });
  });
}

function getAllRequerimentos(req, callback) {
  requerimentoModel.getAllRequerimentos((err, res) => {
    callback(err, res);
  });
}

function getRequerimentoPorID(req, callback) {
  let idReq = req.sanitize(req.params.id);
  requerimentoModel.getRequerimentosPorID(idReq, (err, res) => {
    callback(err, res);
  });
}

function getRequerimentosPorAluno(req, callback) {
  let idAuno = req.sanitize(req.params.idAluno);
  requerimentoModel.getRequerimentosPorAluno(idAuno, (err, res) => {
    callback(err, res);
  });
}

function getRequerimentosPorValidar(req, callback) {
  requerimentoModel.getRequerimentosPorValidar((err, res) => {
    callback(err, res);
  });
}

exports.addRequerimento = addRequerimento;
exports.atualizarRequerimento = atualizarRequerimento;
exports.getAllRequerimentos = getAllRequerimentos;
exports.getRequerimentosPorValidar = getRequerimentosPorValidar;
exports.alterarEstadoRequerimento = alterarEstadoRequerimento;
exports.getRequerimentoPorID = getRequerimentoPorID;
exports.getRequerimentosPorAluno = getRequerimentosPorAluno;