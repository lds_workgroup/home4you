const { check, validator } = require("express-validator/check");
const estudanteModel = require("../models/estudanteModel");
const bcrypt = require("bcryptjs");
const saltRounds = 10;

/**
 * Função responsavel por validar/sanitizar dados, encriptar palavra passe, de um estudante e adicionar à base de dados
 * @param {*} req Request
 * @param {*} callback callback Erro ou false caso os dados não sejam validos
 */
function addEstudante(req, callback) {
  let nome = req.sanitize(req.body.nome);
  let email = req.sanitize(req.body.email);
  let pass = req.sanitize(req.body.pass);
  let nr_estudante = req.sanitize(req.body.nr_estudante);
  let curso = req.sanitize(req.body.curso);
  let ano = req.sanitize(req.body.ano);
  let data_nasc = req.sanitize(req.body.data_nasc);
  let morada = req.sanitize(req.body.morada);

  if (
    nome &&
    email &&
    pass &&
    nr_estudante &&
    curso &&
    ano &&
    data_nasc &&
    morada
  ) {
    bcrypt.hash(pass, saltRounds, function(err, hash) {
      let estudante = new estudanteModel(
        nome,
        email,
        hash,
        nr_estudante,
        curso,
        ano,
        data_nasc,
        morada
      );
      estudanteModel.addEstudante(estudante.thisObject(), err => {
        callback(err);
      });
    });
  } else {
    callback(false);
  }
}

/**
 * Função responsavel por validar/sanitizar dados, encriptar palavra passe, de um estudante e adicionar á base de dados
 * @param req Request
 * @param callback callback Erro ou false caso os dados não sejam validos
 */
function editarEstudante(req, callback) {
  let email = req.sanitize(req.body.email);
  let pass = req.sanitize(req.body.pass);
  let tipo = req.sanitize(req.body.tipo);
  let nr_estudante = req.sanitize(req.body.nr_estudante);
  let curso = req.sanitize(req.body.curso);
  let ano = req.sanitize(req.body.ano);
  let data_nasc = req.sanitize(req.body.data_nasc);
  let morada = req.sanitize(req.body.morada);
  let residencia = req.sanitize(req.body.residencia);
  let estado = req.sanitize(req.body.estado);
  if (
    email &&
    pass &&
    tipo &&
    nr_estudante &&
    curso &&
    ano &&
    data_nasc &&
    morada &&
    estado &&
    residencia
  ) {
    bcrypt.hash(pass, saltRounds, function(err, hash) {
      let query = { nr_estudante: nr_estudante };
      let values = {
        $set: {
          email: email,
          pass: hash,
          tipo: tipo,
          nr_estudante: nr_estudante,
          curso: curso,
          ano: ano,
          data_nasc: data_nasc,
          morada: morada,
          estado: estado,
          residencia: residencia
        }
      };
      estudanteModel.updateEstudante(query, values, err => {
        if (err) {
          callback(err);
        }
        callback(true);
      });
    });
  } else {
    callback(false);
  }
}

/**
 * Função responsavel por alterar o estado do aluno na base de dados
 * @param req Request
 * @param callback callback Erro ou false caso os dados não sejam validos
 */
function alterarEstadoEstudante(req, callback) {
  let idAluno = req.sanitize(req.params.id);
  let novoEstado = req.sanitize(req.params.estado);
  estudanteModel.getEstudanteByNrEstudante(idAluno, (err, resultado) => {
    let query = { nr_estudante: idAluno };
    let values = {
      $set: {
        email: resultado.email,
        pass: resultado.pass,
        tipo: resultado.tipo,
        nr_estudante: resultado.nr_estudante,
        curso: resultado.curso,
        ano: resultado.ano,
        data_nasc: resultado.data_nasc,
        morada: resultado.morada,
        estado: novoEstado,
        residencia: resultado.residencia
      }
    };
    estudanteModel.updateEstudante(query, values, err => {
      if (err) {
        callback(err);
      }
      callback(true);
    });
  });
}

/**
 * Função responsavel por associar uma residencia a um aluno na base de dados
 * @param req Request
 * @param callback callback Erro ou false caso os dados não sejam validos
 */
function associarResidenciaEstudante(req, callback) {
  let idAluno = req.sanitize(req.params.id_aluno);
  let idResidencia = req.sanitize(req.params.residencia);
  estudanteModel.getEstudanteByNrEstudante(idAluno, (err, resultado) => {
    let query = { nr_estudante: idAluno };
    let values = {
      $set: {
        email: resultado.email,
        pass: resultado.pass,
        tipo: resultado.tipo,
        nr_estudante: resultado.nr_estudante,
        curso: resultado.curso,
        ano: resultado.ano,
        data_nasc: resultado.data_nasc,
        morada: resultado.morada,
        estado: resultado.estado,
        residencia: idResidencia
      }
    };
    estudanteModel.updateEstudante(query, values, err => {
      if (err) {
        callback(err);
      }
      callback(true);
    });
  });
}

/**
 * Função responsavel por verificar se a palavra passe introduzida é igual à hash presente na base de dados, permitindo
 * assim o login.
 * @param req Request
 * @param callback Match caso sejam iguas, false caso contrario
 */
function checkData(req, callback) {
  let nr_estudante = req.sanitize(req.body.username);
  let pass = req.sanitize(req.body.pass);
  estudanteModel.getEstudanteByNrEstudante(nr_estudante, (err, resultado) => {
    if (resultado != undefined) {
      let dbPass = resultado.pass;
      if (nr_estudante && pass) {
        bcrypt.compare(pass, dbPass, function(err, match) {
          req.session.username = resultado.nr_estudante;
          req.session.email = resultado.email;
          req.session.tipo = resultado.tipo;
          req.session.nome = resultado.nome;
          req.session.curso = resultado.curso;
          req.session.ano = resultado.ano;
          req.session.data_nasc = resultado.data_nasc;
          req.session.morada = resultado.morada;
          req.session.estado = resultado.estado;
          req.session.residencia = resultado.residencia;
          callback(match);
        });
      } else {
        callback(false);
      }
    } else {
      callback(false);
    }
  });
}

function getEstudanteByNome(req, callback) {
  let nome = req.sanitize(req.body.nome);
  if (nome) {
    estudanteModel.getEstudanteByNome(nome, (err, res) => {
      callback(err, res);
    });
  }
}

function getEstudanteByNrEstudante(req, callback) {
  let nr_estudante = req.sanitize(req.body.nr_estudante);
  if (nr_estudante) {
    estudanteModel.getEstudanteByNrEstudante(nr_estudante, (err, res) => {
      callback(err, res);
    });
  }
}

function getEstudantes(req, callback) {
  estudanteModel.getAllEstudantes((err, res) => {
    callback(err, res);
  });
}

function getEstudantesPorValidar(req, callback) {
  estudanteModel.getEstudantesPorValidar((err, res) => {
    callback(err, res);
  });
}

exports.addEstudante = addEstudante;
exports.editarEstudante = editarEstudante;
exports.alterarEstadoEstudante = alterarEstadoEstudante;
exports.checkData = checkData;
exports.getEstudanteByNome = getEstudanteByNome;
exports.getEstudanteByNrEstudante = getEstudanteByNrEstudante;
exports.getAllEstudantes = getEstudantes;
exports.getEstudantesPorValidar = getEstudantesPorValidar;
exports.associarResidenciaEstudante = associarResidenciaEstudante