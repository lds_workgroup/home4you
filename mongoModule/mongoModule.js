const client = require('mongodb').MongoClient;
const database_obj = {
    mongo_url:'mongodb://localhost:27017',
    dbName:'Home4You'
}


function addDocument(collectionName, document, callback){
    client.connect(database_obj.mongo_url,{ useNewUrlParser: true }, function(err, db){
        if(err){
            throw err;
        }else{
            const dbName = db.db(database_obj.dbName);
            const collection = dbName.collection(collectionName);
            collection.insertOne(document, function (err) {
                callback(err);
                db.close();
            });
        }
    });
}

function findSomething(collectionName, query, callback){
    client.connect(database_obj.mongo_url,{ useNewUrlParser: true }, function(err, db){
        if(err){
            throw err;
        }else{
            const dbName = db.db(database_obj.dbName);
            const collection = dbName.collection(collectionName);
            collection.find(query, function (err, res) {
                if(err) throw err;
                callback(err, res);
                db.close();
            });
        }
    });
}

function findSomethingSpecific(collectionName, query, specification, callback){
    client.connect(database_obj.mongo_url,{ useNewUrlParser: true }, function(err, db){
        if(err){
            throw err;
        }else{
            const dbName = db.db(database_obj.dbName);
            const collection = dbName.collection(collectionName);
            collection.find(query).limit(1).project(specification).toArray(function (err, res){
                callback(err, res[0]);
                db.close;
            });
        }
    });
}

function findAllSomethingSpecific(collectionName, query, specification, callback){
    client.connect(database_obj.mongo_url,{ useNewUrlParser: true }, function(err, db){
        if(err){
            throw err;
        }else{
            const dbName = db.db(database_obj.dbName);
            const collection = dbName.collection(collectionName);
            collection.find(query).project(specification).toArray(function (err, res){
                callback(err, res);
                db.close;
            });
        }
    });
}



function updateSomething(collectionName, query, newValues, callback){
    client.connect(database_obj.mongo_url,{ useNewUrlParser: true }, function(err, db){
        if(err){
            console.log("Erro de Conexão");
        }else{
            const myDB = db.db(database_obj.dbName);
            const collection = myDB.collection(collectionName);
            collection.updateOne(query, newValues, function(res, err){
                callback(err, res);
                db.close();
            });

        }
    });
}

function deleteDocument(collectionName, query, callback){
    client.connect(database_obj.mongo_url,{ useNewUrlParser: true }, function(err, db){
        if(err){
            throw err;
        }else{
            const myDB = db.db(database_obj.dbName);
            const collection = myDB.collection(collectionName);
            collection.deleteOne(query, function(res, err){
                callback(err, res);
                db.close();
            });

        }
    });

}

exports.findAllSomethingSpecific = findAllSomethingSpecific;
exports.deleteDocument=deleteDocument;
exports.updateSomething=updateSomething;
exports.findSomething=findSomething;
exports.addDocument=addDocument;
exports.findSomethingSpecific=findSomethingSpecific;