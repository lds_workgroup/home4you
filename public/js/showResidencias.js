var app = document.getElementById('container');

var request = new XMLHttpRequest();

var url = 'https://localhost:5001/api/residencias';

request.open('GET', url);

request.onload = function () {
    
    var data = JSON.parse(this.response);
    //assim garante que a resposta é positiva
    if (request.status >= 200 && request.status < 400) {
        //para cada residencia encontrada
        data.forEach(residencia => {

            //cria os elementos necessários para mostrar as residencias
            const card = document.createElement('p');
            card.setAttribute('class', 'apresResi');

            //nome residencia
            const nomeRes = document.createElement('h1');
            nomeRes.textContent = residencia.nome;
            console.log(residencia.nome);
            //img residencia
            const imgRes = document.createElement('img');
            imgRes.setAttribute('src', residencia.imageURL);
            imgRes.setAttribute('class', 'imageShow');
            
            //Lugares por ocupar
            const luga = document.createElement('div');
            luga.setAttribute('class', 'desc');
            const lugares = document.createElement('span');
            const lugTex = document.createElement('span');
            lugTex.setAttribute('class', 'textDesc');
            lugTex.textContent = 'Lugares por ocupar: ';
            lugares.textContent = (residencia.totalLug - residencia.ocupLug);

            luga.appendChild(lugTex);
            luga.appendChild(lugares);

            //morada
            const morada = document.createElement('div');
            morada.setAttribute('class', 'desc');
            const moraText = document.createElement('span');
            const mapa = document.createElement('span');
            moraText.setAttribute('class', 'textDesc');
            moraText.textContent = 'Morada: ' ;
            mapa.textContent = residencia.morada;

            morada.appendChild(moraText);
            morada.appendChild(mapa);

            app.appendChild(card);

            card.appendChild(nomeRes);
            card.appendChild(imgRes);
            card.appendChild(luga);
            card.appendChild(morada);
        });

    }else {
        const errorMessage = document.createElement('div');
        errorMessage.textContent = `Error , You suck! Not working IDIOT!`;
        app.appendChild(errorMessage);
      }
}

request.send();