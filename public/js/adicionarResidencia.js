document.getElementById("addResidencia").addEventListener("click", function () {

    var idResidencia = document.getElementById("idResidencia").value;
    var nomeResidencia = document.getElementById("nomeResidencia").value;
    var moradaResidencia = document.getElementById("moradaResidencia").value;
    var totalResidencia = document.getElementById("totalLugares").value;
    var imgResidencia = document.getElementById("imgResidencia").value;

    if (idResidencia == null || nomeResidencia == null || moradaResidencia == null || imgResidencia == null) {
        alert('Preencha os campos todos');
    } else if (totalResidencia <= 0 || totalResidencia == null) {
        alert('Total de Lugares tem que ser superior a 0');
    } else {

        var data = "{ " + "\"id\": \"" + idResidencia + "\", \"nome\": \"" + nomeResidencia + "\", \"morada\": \"" + moradaResidencia + "\", \"totalLug\": \"" + parseInt(totalResidencia) + "\", \"ocupLug\": 0,  \"imageURL\": \"" + imgResidencia + "\" }";

        var request = new XMLHttpRequest();

        var url = 'https://localhost:5001/api/residencias';

        request.open('POST', url);
        request.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        request.onload = function () {
            // do something to response
            console.log(this.responseText);
        };
        
        request.send(data);
    }
    window.location.href = '/menuAdmin';
});