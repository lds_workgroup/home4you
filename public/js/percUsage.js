var canvas = document.getElementById("myPieChart");
var ctx = canvas.getContext('2d');

var quartosOcupados = 0;
var quartosOcupar = 0;
var totalQuartos = 0;

var request = new XMLHttpRequest();

var url = 'https://localhost:5001/api/residencias';

request.open('GET', url);

request.onload = function () {

    var data = JSON.parse(this.response);
    if (request.status >= 200 && request.status < 400) {
        data.forEach(residencia => {
            quartosOcupados += residencia.ocupLug;
            quartosOcupar += (residencia.totalLug - residencia.ocupLug);
            totalQuartos += residencia.totalLug;
        });
    }

    makeChart();

}

function makeChart() {

    var percOcupado = 0;
    var percOcupar = 0;
    //calc percentage
    percOcupado = (quartosOcupados / totalQuartos) * 100;
    percOcupar = (quartosOcupar / totalQuartos) * 100;

    data = {
        datasets: [{
            fill: true,
            backgroundColor: [
                'red',
                'green'],
            data: [percOcupado, percOcupar],
            //border
            borderColor:	['black', 'black'],
            borderWidth: [2,2]
        }],
    
        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
            'Ocupado',
            'Por Ocupar'
        ]
    };

    var options = {
        title: {
            display: true,
            text: "Ocupação Residencias",
            position: 'top'
        },
        rotation: -0.7 * Math.PI
};

    //Cria o chart
    var myPieChart = new Chart(ctx,{
        type: 'pie',
        data: data,
        options: options
    });
}

request.send();