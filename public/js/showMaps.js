function myMap() {
    var mapProp = {
        center: new google.maps.LatLng(41.3660461, -8.1955125),
        zoom: 13,
    };
    var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

    var geocoder = new google.maps.Geocoder();
    addAddressMarkers(geocoder, map);
}

function addAddressMarkers(geocoder, resultsMap) {

    var request = new XMLHttpRequest();

    var url = 'https://localhost:5001/api/residencias';

    request.open('GET', url);

    request.onload = function () {

        var data = JSON.parse(this.response);

        if (request.status >= 200 && request.status < 400) {
            data.forEach(residencia => {

                geocoder.geocode({ 'address': residencia.morada }, function (results, status) {
                    if (status === 'OK') {
                        var infowindow = new google.maps.InfoWindow({
                            content: residencia.nome
                        });
                        var marker = new google.maps.Marker({
                            map: resultsMap,
                            position: results[0].geometry.location,
                            title: residencia.nome
                        });

                        marker.addListener('click', function () {
                            infowindow.open(resultsMap, marker);
                        });
                    } else {
                        alert('Geocode was not successful for the following reason: ' + status);
                    }
                });

            });
        }else{
            console.log('Something is Wrong. API connection failled!');
        }
    }

    request.send();
}