const express = require("express");
const http = require("https");
const router = express.Router();
const adminController = require("../controllers/adminController");
const funcionarioController = require("../controllers/funcionarioController");
const estudantesController = require("../controllers/estudanteController");
const requerimentoController = require("../controllers/requerimentoController");

/**
 * -----INCIO PEDIDOS GET-----
 */
router.get("/login", function(req, res) {
  if (req.session.username) {
    res.render("login", {
      username: req.session.username
    });
  }
});

router.get("/registo", function(req, res) {
  res.render("registo");
});


//Pedidos Administrador
router.get("/menuAdmin", function(req, res) {
  res.render("menuAdmin");
});

router.get("/adminAdicionarResidencias", function(req, res) {
  res.render("adminAdicionarResidencias");
});

router.get("/adminVerResidencias", function(req, res) {
  res.render("adminVerResidencias");
});

router.get("/adminCriarFuncionario", function(req, res) {
  res.render("adminCriarFuncionario");
});

router.get("/adminRemoverFuncionario", function(req, res) {
  res.render("adminRemoverFuncionario");
});

router.get("/adminValidarAluno", function(req, res) {
  res.render("adminValidarAluno");
});

router.get("/adminValidarPedido", function(req, res) {
  res.render("adminValidarPedido");
});

router.get("/getTodosFunc", (req, res) => {
  funcionarioController.getFuncionarios(req, (err, documents) => {
    if (err) {
      console.log(err);
    } else {
      res.json(documents);
    }
  });
});

router.get("/getAlunosPorValidar", (req, res) => {
  estudantesController.getEstudantesPorValidar(req, (err, documents) => {
    if (err) {
      console.log(err);
    } else {
      res.json(documents);
    }
  });
});

router.get("/getRequerimentosPorValidar", (req, res) => {
  requerimentoController.getRequerimentosPorValidar(req, (err, documents) => {
    if (err) {
      console.log(err);
    } else {
      res.json(documents);
    }
  });
});

router.get("/getRequerimentosPorAluno", (req, res) => {
  req.params.idAluno = req.session.username;
  requerimentoController.getRequerimentosPorAluno(req, (err, documents) => {
    if (err) {
      console.log(err);
    } else {
      res.json(documents);
    }
  });
});


//Pedidos Funcionario
router.get("/menuFuncionario", function(req, res) {
  res.render("menuFuncionario");
});

router.get("/funcVerResidencias", function(req, res) {
  res.render("funcVerResidencias");
});

router.get("/funcValidarAluno", function(req, res) {
  res.render("funcValidarAluno");
});

router.get("/funcValidarRequerimentos", function(req, res) {
  res.render("funcValidarRequerimentos");
});



//Pedidos Aluno
router.get("/menuAluno", function(req, res) {
  res.render("menuAluno");
});

router.get("/alunoVerResidencias", function(req, res) {
  res.render("alunoVerResidencias");
});

router.get("/alunoContaPorValidar", function(req, res) {
  res.render("alunoContaPorValidar");
});

router.get("/alunoContaInvalida", function(req, res) {
  res.render("alunoContaInvalida");
});

router.get("/alunoFazerPedido", function(req, res) {
  res.render("alunoFazerPedido");
});

router.get("/alunoVerEstadoPedido", function(req, res){
  res.render("alunoVerEstadoPedido");
});

router.get("/alunoPerfil", function(req, res){
  res.render("alunoPerfil");
});

router.get("/getAlunoID", (req, res) => {
  req.body.nr_estudante = req.session.username;
  estudantesController.getEstudanteByNrEstudante(req, (err, documents) => {
    if (err) {
      console.log(err);
    } else {
      res.json(documents);
    }
  });
});

/**
 * -----INCIO PEDIDOS POST-----
 */
router.post("/login", function(req, res) {
  if (req.body.tipo == 1) {
    adminController.checkData(req, resultado => {
      if (resultado) {
        res.redirect("menuAdmin");
      } else {
        res.send("Credenciais erradas");
      }
    });
  } else if (req.body.tipo == 2) {
    funcionarioController.checkData(req, resultado => {
      if (resultado) {
        res.redirect("menuFuncionario");
      } else {
        res.send("Credenciais erradas");
      }
    });
  } else if (req.body.tipo == 3) {
    estudantesController.checkData(req, resultado => {
      if (resultado) {
        if (req.session.estado == 0) {
          res.redirect("alunoContaPorValidar");
        }
        if (req.session.estado == 1) {
          res.redirect("menuAluno");
        }
        if (req.session.estado == 2) {
          res.redirect("alunoContaInvalida");
        }
      } else {
        res.send("Credenciais erradas");
      }
    });
  }
});

router.post("/registo", function(req, res) {
  estudantesController.getEstudanteByNrEstudante(req, function(err, user) {
    if (err) {
      next(err);
    } else if (user) {
      res.send("Estudante já registado");
    } else {
      let data = estudantesController.addEstudante(req, err => {
        if (err || err === false) {
          res.end("Erro: " + err);
        } else {
          //res.send("Estudante adicionado com sucesso");
        }
      });
      res.redirect("/");
    }
  });
});

router.post("/adminCriarFuncionario", function(req, res) {
  funcionarioController.getFuncionarioByUsername(req, function(err, user) {
    if (err) {
      next(err);
    } else if (user) {
      res.send("Funcionário já existe");
    } else {
      let data = funcionarioController.addFuncionario(req, err => {
        if (err || err === false) {
          res.end("Erro: " + err);
        }
      });
      res.redirect("menuAdmin");
    }
  });
});

router.post("/alunoFazerPedido", function(req, res) {
  requerimentoController.addRequerimento(req, function(err, res) {
    if (err || err === false) {
      res.end("Erro: " + err);
    }
  });
  res.redirect("menuAluno");
});

//metodo para eliminar funcionario por id
router.delete("/eliminaFunc/:id", function(req, res) {
  funcionarioController.eliminarFuncionario(req, (err, documents) => {
    if (err) {
      console.log(err);
    } else {
      res.json(documents);
    }
  });
});

router.post("/logout", function(req, res) {
  req.session.destroy();
  res.redirect("/");
});

/**
 * -----INCIO PEDIDOS PUT-----
 */

router.put("/validaAluno/:id", function(req, res) {
  req.params.estado = 1;
  estudantesController.alterarEstadoEstudante(req, (err, documents) => {
    if (err) {
      console.log(err);
    } else {
      res.json(documents);
    }
  });
});

router.put("/invalidaAluno/:id", function(req, res) {
  req.params.estado = 2;
  estudantesController.alterarEstadoEstudante(req, (err, documents) => {
    if (err) {
      console.log(err);
    } else {
      res.json(documents);
    }
  });
});

router.put("/validaRequerimento/:id/:id_aluno", function(req, res) {
  req.body.nr_estudante = req.params.id_aluno;
  estudantesController.getEstudanteByNrEstudante(req, (err, resultado) => {
    if (err) {
      console.log(err);
    } else {
      //verifica se o estudante tem alguma residência associada, se não tiver procura e associa uma, caso contrario torna o requerimento inválido
      if (resultado.residencia == null) {
        req.params.estado = 1;
        //altera o estado do requerimento para Validado
        requerimentoController.alterarEstadoRequerimento(req, (err, documents) => {
          //opções para pedido Get
            var options = {
              host: "localhost",
              port: 5001,
              path: "/api/Residencias",
              rejectUnauthorized: false,
              requestCert: true,
              agent: false
            };
            //pedido para obter residencias
            var reques = http.get(options, function(res) {
              console.log("STATUS: " + res.statusCode);
              console.log("HEADERS: " + JSON.stringify(res.headers));
              //variavel que recebe as residencias
              var bodyChunks = [];
              res.on("data", function(chunk) {
                //adiciona as residencias à variavel
                bodyChunks.push(chunk);
                }).on("end", function() {
                  var body = Buffer.concat(bodyChunks);
                  //passa as Residencias de objeto para json
                  var stringResidencias = JSON.parse(body);
                  for (var i = 0; i < stringResidencias.length; i++) {
                    //verifica se a residencia tem lugares disponiveis
                    if (stringResidencias[i].ocupLug < stringResidencias[i].totalLug) {
                      var lugares = stringResidencias[i].ocupLug + 1;
                      //data da residencia para fazer o update do lugar ocupado
                      var data = "{ " + "\"id\": \"" + stringResidencias[i].id + "\", \"nome\": \"" + stringResidencias[i].nome + "\", \"morada\": \"" + stringResidencias[i].morada + "\", \"totalLug\": \"" + stringResidencias[i].totalLug + "\", \"ocupLug\": " + lugares + ", \"imageURL\": \"" + stringResidencias[i].imageURL + "\" }";
                      //url para fazer PUT
                      var url = '/api/Residencias/' + stringResidencias[i].id;
                      //Opções do PUT Request para dar update na residencia
                      var optionsPut = {
                        host: "localhost",
                        port: 5001,
                        path: url,
                        method: "PUT",
                        headers: {
                          "Content-type": "application/json"
                        },
                        rejectUnauthorized: false,
                        requestCert: true,
                        agent: false
                      };
                      //Faz PUT request
                      var requestPut = http.request(optionsPut, resi => {
                        console.log('statusCode: ' + res.statusCode);
                        resi.setEncoding('utf8');

                        resi.on("data", d => {
                          process.stdout.write(d);
                        });
                      });

                      requestPut.on("error", error => {
                        console.error(error);
                      });

                      requestPut.write(data);
                      requestPut.end();

                      req.params.residencia = stringResidencias[i].id;
                      estudantesController.associarResidenciaEstudante(req, (err, documents) => {
                        if (err) {
                          console.log(err);
                        } else {
                          res.json(documents);
                        }
                      });
                      //para o ciclo for pois já encontrou uma residencia para o aluno
                      break;
                    }
                  }
                });
            });
            reques.on("error", function(e) {
              console.log("ERROR: " + e.message);
            });
          }
        );
      } else {
        req.params.estado = 2;
        //altera o estado do requerimento para invalido porque o aluno ja esta a habitar uma residencia
        requerimentoController.alterarEstadoRequerimento(req,(err, documents) => {
            if (err) {
              console.log(err);
            } else {
              res.json(documents);
            }
          }
        );
      }
    }
  });
});

router.put("/invalidaRequerimento/:id", function(req, res) {
  req.params.estado = 2;
  //altera o estado do requerimento para invalido
  requerimentoController.alterarEstadoRequerimento(req,(err, documents) => {
    if (err) {
      console.log(err);
    } else {
      res.json(documents);
    }
  });
});


module.exports = router;