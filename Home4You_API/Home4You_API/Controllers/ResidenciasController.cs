﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Home4You_API.Data;
using Home4You_API.ViewModels;
using Microsoft.AspNetCore.Cors;

namespace Home4You_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("Home4YouPolicy")]
    public class ResidenciasController : ControllerBase
    {
        private readonly HomeContext _context;

        public ResidenciasController(HomeContext context)
        {
            _context = context;
        }

        // GET: api/Residencias
        [HttpGet]
        public IEnumerable<Residencia> GetResidencias()
        {
            return _context.Residencias;
        }
        
        // GET: api/Residencias/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetResidencia([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var residencia = await _context.Residencias.FindAsync(id);

            if (residencia == null)
            {
                return NotFound();
            }

            return Ok(residencia);
        }

        // PUT: api/Residencias/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutResidencia([FromRoute] string id, [FromBody] Residencia residencia)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != residencia.id)
            {
                return BadRequest();
            }

            _context.Entry(residencia).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ResidenciaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Residencias
        [HttpPost]
        public async Task<IActionResult> PostResidencia([FromBody] Residencia residencia)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Residencias.Add(residencia);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetResidencia", new { id = residencia.id }, residencia);
        }

        // DELETE: api/Residencias/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteResidencia([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var residencia = await _context.Residencias.FindAsync(id);
            if (residencia == null)
            {
                return NotFound();
            }

            _context.Residencias.Remove(residencia);
            await _context.SaveChangesAsync();

            return Ok(residencia);
        }

        private bool ResidenciaExists(string id)
        {
            return _context.Residencias.Any(e => e.id == id);
        }
    }
}