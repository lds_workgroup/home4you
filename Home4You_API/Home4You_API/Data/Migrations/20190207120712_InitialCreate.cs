﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Home4You_API.Data.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Alunos",
                columns: table => new
                {
                    nMecanografico = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Alunos", x => x.nMecanografico);
                });

            migrationBuilder.CreateTable(
                name: "Residencias",
                columns: table => new
                {
                    id = table.Column<string>(nullable: false),
                    nome = table.Column<string>(nullable: true),
                    morada = table.Column<string>(nullable: true),
                    totalLug = table.Column<int>(nullable: false),
                    ocupLug = table.Column<int>(nullable: false),
                    imageURL = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Residencias", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Alunos");

            migrationBuilder.DropTable(
                name: "Residencias");
        }
    }
}
