﻿using Home4You_API.ViewModels;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Home4You_API.Data
{
    public class HomeData
    {
        public static void Initialize(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<HomeContext>();
                context.Database.EnsureCreated();

                // Look for any residencias && alunos
                if (context.Residencias != null && context.Residencias.Any())
                {
                    if (context.Alunos != null && context.Alunos.Any())
                    {
                        return;   // DB has already been seeded
                    }
                }

                //else generate the db informations
                var residencias = GetResidencias().ToArray();
                context.Residencias.AddRange(residencias);
                context.SaveChanges();

                var alunos = GetAlunos().ToArray();
                context.Alunos.AddRange(alunos);
                context.SaveChanges();
            } 
        }

        public static List<Residencia> GetResidencias()
        {
            List<Residencia> residencias = new List<Residencia>()
            {
                //Falta incluir residencias
                new Residencia {id = "RES001", nome="Residencia 1", morada ="Praça da República 76, 4610-116 Felgueiras", totalLug = 10, ocupLug = 0  ,imageURL="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTs72__SQBilzHHdQD9sk0aBnQ3ySB8MkKlTMGuxWpRr9j_-cZt"}
            };
            return residencias;
        }

        public static List<Aluno> GetAlunos()
        {
            List<Aluno> alunos = new List<Aluno>()
            {
                //Falta incluir alunos
                new Aluno { nMecanografico = "8150442"}
            };
            return alunos;
        }
    }
}
