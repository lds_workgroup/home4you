﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Home4You_API.ViewModels
{
    public class Residencia
    {
        [Key]
        public string id { get; set;}
        public string nome { get; set; }
        public string morada { get; set; }
        public int totalLug { get; set; }
        public int ocupLug { get; set; }
        public string imageURL { get; set; }
    }
}
