﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Home4You_API.ViewModels
{
    public class Aluno
    {
        [Key]
        public string nMecanografico { get; set; }
    }
}
