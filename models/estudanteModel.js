const mongo = require('../mongoModule/mongoModule');
const collection = 'estudantes';

class estudante {
    constructor(nome, email, pass, nr_estudante, curso, ano, data_nasc, morada) {
        this._nome = nome;
        this._email = email;
        this._pass = pass;
        this._nr_estudante = nr_estudante;
        this._curso = curso;
        this._ano = ano;
        this._data_nasc = data_nasc;
        this._morada = morada;
        this._residencia = null;
        this._estado = 0;
        this._tipo = 3;
    }

    get nome() {
        return this._nome;
    }

    get email() {
        return this._email;
    }

    get pass() {
        return this._pass;
    }

    get nr_estudante() {
        return this._nr_estudante;
    }

    get curso() {
        return this._curso;
    }

    get ano() {
        return this._ano;
    }

    get data_nasc() {
        return this._data_nasc;
    }

    get morada() {
        return this._morada;
    }

    get residencia() {
        return this._residencia;
    }

    get estado() {
        return this._estado;
    }

    get tipo() {
        return this._tipo;
    }

    thisObject() {
        let newObject = {
            nome: this._nome, email: this._email,
            pass: this._pass, nr_estudante: this._nr_estudante, curso: this._curso,
            ano: this._ano, data_nasc: this._data_nasc, morada: this._morada,
            residencia: this._residencia, estado: this._estado, tipo: this._tipo
        };
        return newObject;
    }

    /**
     * metodo que adiciona uma instancia de estudante à base de dados
     * @param {*} estudante 
     * @param {*} callback 
     */
    static addEstudante(estudante, callback) {
        mongo.addDocument(collection, estudante, (err) => {
            callback(err);
        });
    }

    /**
     * metodo que devolve uma instancia de estudante atraves do email
     * @param {*} nr_estudante
     * @param {*} callback 
     */
    static getEstudanteByNrEstudante(nr_estudante, callback) {
        let query = { nr_estudante: nr_estudante };
        let spec = {};
        mongo.findSomethingSpecific(collection, query, spec, (err, res) => {
            callback(err, res);
        });
    }

    /**
     * metodo que devolve uma instancia de estudante atraves do nome
     * @param {*} nome 
     * @param {*} callback 
     */
    static getEstudanteByNome(nome, callback) {
        let query = { nome: nome };
        let spec = {};
        mongo.findAllSomethingSpecific(collection, query, spec, (err, res) => {
            callback(err, res);
        });
    }

    /**
     * metodo que apaga uma instancia de estudante à base de dados
     * @param {*} nr_estudante 
     * @param {*} callback 
     */
    static deleteEstudante(nr_estudante, callback) {
        let query = { nr_estudante: nr_estudante };
        mongo.deleteDocument(collection, query, (err, res) => {
            callback(err, res);
        });
    }

    /**
     * metodo que atualiza uma instancia de estudante à base de dados
     * @param {*} query 
     * @param {*} values 
     * @param {*} callback 
     */
    static updateEstudante(query, values, callback) {
        mongo.updateSomething(collection, query, values, (err, res) => {
            callback(err, res);
        });
    }

    /**
     * metodo que devolve todas as instancias de estudantes
     * @param {*} callback 
     */
    static getAllEstudantes(callback) {
        let query = {};
        let spec = { nome: 1, nr_estudante: 1, _id: 0 };
        mongo.findAllSomethingSpecific(collection, query, spec, (err, res) => {
            callback(err, res);
        });
    }

    /**
     * metodo que devolve todas as instancias de estudantes
     * @param {*} callback 
     */
    static getEstudantesPorValidar(callback) {
        let query = {estado: 0};
        let spec = { nome: 1, nr_estudante: 1, _id: 0 };
        mongo.findAllSomethingSpecific(collection, query, spec, (err, res) => {
            callback(err, res);
        });
    }
}
module.exports = estudante;