const mongo = require("../mongoModule/mongoModule");
var ObjectId = require("mongodb").ObjectID;
const collection = "requerimentos";

class requerimento {
  constructor(id_aluno, titulo, descricao, data) {
    this._id_aluno = id_aluno;
    this._titulo = titulo;
    this._descricao = descricao;
    this._data = data;
    this._estado = 0;
  }

  get id_aluno() {
    return this._id_aluno;
  }

  get titulo() {
    return this._titulo;
  }

  get descricao() {
    return this._descricao;
  }

  get data() {
    return this._data;
  }

  get estado() {
    return this._estado;
  }

  thisObject() {
    let newObject = {
      id_aluno: this._id_aluno,
      titulo: this._titulo,
      descricao: this._descricao,
      data: this._data,
      estado: this._estado
    };
    return newObject;
  }

  /**
   * metodo que adiciona uma instancia de requerimento à base de dados
   * @param {*} requerimento
   * @param {*} callback
   */
  static addRequerimento(requerimento, callback) {
    mongo.addDocument(collection, requerimento, err => {
      callback(err);
    });
  }

  /**
   * metodo que apaga uma instancia de requerimento à base de dados
   * @param {*} id
   * @param {*} callback
   */
  static deleteRequerimento(id, callback) {
    let query = { id: id };
    mongo.deleteDocument(collection, query, (err, res) => {
      callback(err, res);
    });
  }

  /**
   * metodo que atualiza uma instancia de requerimento à base de dados
   * @param {*} query
   * @param {*} values
   * @param {*} callback
   */
  static updateRequerimento(query, values, callback) {
    mongo.updateSomething(collection, query, values, (err, res) => {
      callback(err, res);
    });
  }

  /**
   * metodo que devolve todas os requerimentos por validar
   * @param {*} callback
   */
  static getRequerimentosPorValidar(callback) {
    let query = { estado: 0 };
    let spec = { _id: 1, id_aluno: 1, titulo: 1, descricao: 1 };
    mongo.findAllSomethingSpecific(collection, query, spec, (err, res) => {
      callback(err, res);
    });
  }

  /**
   * metodo que devolve todas os requerimentos por validar
   * @param {*} callback
   */
  static getRequerimentosPorAluno(idAluno, callback) {
    let query = { id_aluno: idAluno };
    let spec = { };
    mongo.findAllSomethingSpecific(collection, query, spec, (err, res) => {
      callback(err, res);
    });
  }

  /**
   * metodo que devolve todas o requerimento que procura
   * @param {*} callback
   */
  static getRequerimentosPorID(idReq, callback) {
    let id = ObjectId(idReq);
    let query = { _id: id };
    let spec = {};
    mongo.findSomethingSpecific(collection, query, spec, (err, res) => {
      callback(err, res);
    });
  }

  /**
   * metodo que devolve todas as instancias de requerimento
   * @param {*} callback
   */
  static getAllRequerimentos(callback) {
    let query = {};
    let spec = { id_aluno: 1, titulo: 1, _id: 0 };
    mongo.findAllSomethingSpecific(collection, query, spec, (err, res) => {
      callback(err, res);
    });
  }
}
module.exports = requerimento;
