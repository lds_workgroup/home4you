const mongo = require('../mongoModule/mongoModule');
const collection = 'admins';

class admin{
    constructor(username, pass){
        this._username = username;
        this._pass = pass;
        this._tipo = 1;
    }

    get username(){
        return this._username;
    }

    get pass(){
        return this._pass;
    }

    get tipo(){
        return this._tipo;
    }

    thisObject(){
        let newObject = {username: this._username, pass: this._pass, tipo: this._tipo};
        return newObject;
    }

    /**
     * metodo que adiciona uma instancia de admin à base de dados
     * @param {*} admin 
     * @param {*} callback 
     */
    static addAdmin(admin, callback){
        mongo.addDocument( collection, admin, (err) =>{
            callback(err);
        });
    }

    /**
     * metodo que devolve uma instancia de admin atraves do username
     * @param {*} username
     * @param {*} callback 
     */
    static getAdminByUsername(username, callback){
        let query = { username : username  };
        let spec = {};
        mongo.findSomethingSpecific(collection, query, spec, (err, res)=>{
            callback(err, res);
        });
    }
    

    /**
     * metodo que apaga uma instancia de admin à base de dados
     * @param {*} username 
     * @param {*} callback 
     */
    static deleteAdmin(username, callback){
        let query = { username : username  };
        mongo.deleteDocument(collection, query, (err, res) => {
            callback(err, res);
        });
    }

    /**
     * metodo que devolve todas as instancias de admin
     * @param {*} callback 
     */
    static getAllAdmins(callback){
        let query = {};
        let spec = {username: 1,_id: 0};
        mongo.findAllSomethingSpecific(collection, query, spec, (err, res)=>{
            callback(err,res);
        });
    }
    

}
module.exports = admin;