const mongo = require('../mongoModule/mongoModule');
const collection = 'funcionarios';

class funcionario{
    constructor(nome, username, pass){
        this._nome = nome;
        this._username = username;
        this._pass = pass;
        this._tipo = 2;
    }

    get nome(){
        return this._nome;
    }

    get username(){
        return this._username;
    }

    get pass(){
        return this._pass;
    }

    get tipo(){
        return this._tipo;
    }

    thisObject(){
        let newObject = {nome: this._nome, username: this._username, pass: this._pass, tipo: this._tipo};
        return newObject;
    }

    /**
     * metodo que adiciona uma instancia de funcionario à base de dados
     * @param {*} funcionario 
     * @param {*} callback 
     */
    static addFuncionario(funcionario, callback){
        mongo.addDocument(collection, funcionario, (err) =>{
            callback(err);
        });
    }

    /**
     * metodo que devolve uma instancia de funcionario atraves do username
     * @param {*} username
     * @param {*} callback 
     */
    static getFuncionarioByUsername(username, callback){
        let query = { username : username  };
        let spec = {};
        mongo.findSomethingSpecific(collection, query, spec, (err, res)=>{
            callback(err, res);
        });
    }
    

    /**
     * metodo que devolve uma instancia de funcionario atraves do nome
     * @param {*} nome 
     * @param {*} callback 
     */
    static getFuncionarioByNome(nome, callback){
        let query = {nome: nome};
        let spec = {};
        mongo.findAllSomethingSpecific(collection, query, spec, (err, res)=>{
           callback(err,res);
        });
    }

    /**
     * metodo que apaga uma instancia de funcionario à base de dados
     * @param {*} username 
     * @param {*} callback 
     */
    static deleteFuncionario(username, callback){
        let query = { username : username  };
        mongo.deleteDocument(collection, query, (err, res) => {
            callback(err, res);
        });
    }

    /**
     * metodo que atualiza uma instancia de funcionario à base de dados
     * @param {*} query 
     * @param {*} values 
     * @param {*} callback 
     */
    static updateFuncionario(query, values, callback){
        mongo.updateSomething(collection, query, values, (err, res)=>{
            callback(err, res);
        });
    }

    /**
     * metodo que devolve todas as instancias de funcionario
     * @param {*} callback 
     */
    static getAllFuncionarios(callback){
        let query = {};
        let spec = {nome: 1, username: 1,_id: 0};
        mongo.findAllSomethingSpecific(collection, query, spec, (err, res)=>{
            callback(err,res);
        });
    }

}
module.exports = funcionario;